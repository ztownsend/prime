'use strict';


angular.module('primes', [])


.controller('MainCtrl', function($scope) {

  var store = {
    maxCanDisplay: 30,
    qtyToFind: 10,
    showGrid: true,
    showSinglePrime: false
  }
  $scope.store = store;

  $scope.clickDisplayPrimes = function() {
    $scope.displayPrimes(store.qtyToFind);
  }

  $scope.displayPrimes = function(qtyToFind) {
    store.qtyToFind = qtyToFind;
    if(qtyToFind>0 && !store.calculating) {
      console.log(qtyToFind);

        // Don't bother array grid of primes if more than can display as waste of memory
        if (qtyToFind <= store.maxCanDisplay) {
          var primes = [];
        }
        store.calculating = true;
        store.showGrid = false;

        var i = 0, numToTest = 1;

        while (i < store.qtyToFind) {
          numToTest++;
          if ($scope.isPrime(numToTest)) {
              if (primes) primes.push(numToTest);
              i++;
            }
          
        }
        store.lastPrime = numToTest;

        if(qtyToFind <= store.maxCanDisplay) {
          store.primes = primes;
        }

        qtyToFind <= store.maxCanDisplay ? store.showGrid = true : store.showGrid = false;
        store.showSinglePrime = !store.showGrid;
        
        store.qty = store.qtyToFind;
        store.calculating = false;
        return store;
    }
  }
  
  $scope.isPrime = function(num) {
    for (var i=2; i<num; i++) {
      if(num%i == 0) return false;
    }
    return true;
  }

})







.directive('primes', function () {
    return {
      restrict: 'A',
      templateUrl: 'templates/primes.html',
       
        // Display grid
        link: function($scope, element, attrs) {
          var store = $scope.displayPrimes($scope.store.qtyToFind);
        }
      }
})









