'use strict';

describe('MainCtrl module', function() {
	var $timeout = null, $scope = null, $compile = null, element, MainCtrl = null, store = {};

	beforeEach(module('primes'));
	beforeEach(module('templates/primes.html'));

	beforeEach( inject(function($rootScope, $controller, _$timeout_, _$compile_) {
		$scope = $rootScope.$new();
		$timeout = _$timeout_;
		$compile = _$compile_;
		MainCtrl = $controller('MainCtrl', { $scope: $scope });
   		var template = '<div primes ></div>';
   		element = $compile(template)($scope);
   		$scope.$digest();
	}));

	it('should initiate correctly', function(){
		expect($scope).toBeDefined();
		expect($scope.store).toBeDefined();
	});
	
	it('should correctly identify if a number is prime', function(){
		expect($scope.isPrime(89)).toBeTruthy();
		expect($scope.isPrime(4)).toBeFalsy();
	});

	it('should set up the default primes directive on the page correctly', function(){
		expect(element).toBeDefined();
		expect(element.find('h2')).toBeDefined();
		expect(element.find('div')[1]).toBeDefined();
	});

	it('should create 17 divs - 2 for the primes, and 15 for the muliplieds', function(){
		expect(element.find('div').find('div').attr('class').length).toEqual(17);
	});

	it('should set text to show the last prime to be 29', function(){
		expect(element.find('h2').text()).toEqual('29');
	});

	it('should display the result of two primes (29 & 29) multiplied to be 841 ', function(){
		expect($(element.find('span')[119]).text()).toEqual('841');
	});

	it('should calculate a new set of 5 primes', function(){
		var store2 = $scope.displayPrimes(5);
		expect(store2.lastPrime).toEqual(11);
		expect(store2.showGrid).toBeTruthy();
		expect(store2.showSinglePrime).toBeFalsy();
	});

	it('should click the button to create a new set of primes', function(){
		$scope.store.qtyToFind = 7;
		$scope.clickDisplayPrimes();
		expect($scope.store.lastPrime).toEqual(17);
		expect($scope.store.showGrid).toBeTruthy();
		expect($scope.store.showSinglePrime).toBeFalsy();
	});

	it('should not display the grid when there are too many primes', function(){
		var store3 = $scope.displayPrimes($scope.store.maxCanDisplay + 1);
		expect(store3.showGrid).toBeFalsy();
		expect(store3.showSinglePrime).toBeTruthy();
		expect(store3.lastPrime).toEqual(127);
	});

	it('should ignore a qtyToFind value of zero', function(){
		var store4 = $scope.displayPrimes(0);
		expect(store4).toBeUndefined();
	});


});

