# Primes #

A simple Angular SPA to illustrate calculating primes, a custom directive, and unit tests using Jasmine/Karma.

## Installation ##

You need bower and node/npm and Chrome installed on your system to run the tests.

1. Clone the repo
2. In the root folder of the repo, run 'bower install' and 'npm install' which will install the dependancies for the app, and the tests.
3. Launch index.html with 'gulp' or any other webserver. Note: a webserver is mandatory.
4. Run the tests with 'karma start'
5. A test coverage report will be generated at './coverage/Chrome {xxxx}/index.html'